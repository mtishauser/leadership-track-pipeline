import nltk
from nltk.classify import NaiveBayesClassifier
from nltk.classify.util import accuracy


def format_sentence(sent):
    return {word: True for word in nltk.word_tokenize(sent)}


pos = []

with open("./training/pos_tweets.txt") as f:
    for i in f:
        pos.append([format_sentence(i), 'pos'])

neg = []
with open("./training/neg_tweets.txt") as f:
    for i in f:
        neg.append([format_sentence(i), 'neg'])

# next, split labeled data into the training and test data
training = pos[:int((.8)*len(pos))] + neg[:int((.8)*len(neg))]
test = pos[int((.8)*len(pos)):] + neg[int((.8)*len(neg)):]

classifier = NaiveBayesClassifier.train(training)
print(accuracy(classifier, test))

classifier.show_most_informative_features()


example1 = "Hooray, I love fixing bug"
print(example1)
print(classifier.classify(format_sentence(example1)))
example2 = "I don’t like cats."
print(example2)
print(classifier.classify(format_sentence(example2)))
